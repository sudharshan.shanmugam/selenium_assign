package Automation;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class OrangeHrmAssesment {

	   public static void main(String[] args) throws AWTException, InterruptedException {
	        WebDriver driver = new ChromeDriver();
	        driver.manage().window().maximize();
	        
	        driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");
	        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
	        driver.findElement(By.name("username")).sendKeys("Admin");
	        driver.findElement(By.name("password")).sendKeys("admin123");
	        driver.findElement(By.xpath("//button[@type='submit']")).click();
	        driver.findElement(By.xpath("//span[text()='Admin']")).click();
	        driver.findElement(By.xpath("//button[@class='oxd-button oxd-button--medium oxd-button--secondary']")).click();
	        driver.findElement(By.xpath("//label[text()='User Role']/../../descendant::i")).click();
	        Robot r = new Robot();
	        r.keyPress(KeyEvent.VK_DOWN);
	        r.keyRelease(KeyEvent.VK_DOWN);
	        r.keyPress(KeyEvent.VK_ENTER);
	        r.keyRelease(KeyEvent.VK_ENTER);
	        r.keyPress(KeyEvent.VK_TAB);
	        r.keyRelease(KeyEvent.VK_TAB);
	        driver.findElement(By.xpath("//label[text()='Status']/../../descendant::i")).click();    
	        r.keyPress(KeyEvent.VK_DOWN);
	        r.keyRelease(KeyEvent.VK_DOWN);
	        r.keyPress(KeyEvent.VK_ENTER);
	        r.keyRelease(KeyEvent.VK_ENTER);
	        r.keyPress(KeyEvent.VK_TAB);
	        r.keyRelease(KeyEvent.VK_TAB);
	        
	        driver.findElement(By.xpath("//input[@placeholder='Type for hints...']")).sendKeys("joe Root");
	        Thread.sleep(3000);
	        r.keyPress(KeyEvent.VK_DOWN);
	        r.keyRelease(KeyEvent.VK_DOWN);
	        r.keyPress(KeyEvent.VK_DOWN);
	        r.keyRelease(KeyEvent.VK_DOWN);
	        r.keyPress(KeyEvent.VK_ENTER);
	        r.keyRelease(KeyEvent.VK_ENTER);
	        driver.findElement(By.xpath("//label[text()='Username']/../../descendant::input")).sendKeys("Joe Root");
	        driver.findElement(By.xpath("//label[text()='Password']/../../descendant::input")).sendKeys("Password@123");
	        driver.findElement(By.xpath("//label[text()='Confirm Password']/../../descendant::input")).sendKeys("Password@123");
	        driver.findElement(By.xpath("//button[@type='submit']")).click();
	        driver.close();
	    }

	
}
